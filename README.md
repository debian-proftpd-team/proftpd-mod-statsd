proftpd-mod_statsd
==================

Status
------
[![Build Status](https://travis-ci.org/Castaglia/proftpd-mod_statsd.svg?branch=master)](https://travis-ci.org/Castaglia/proftpd-mod_statsd)
[![Coverage Status](https://coveralls.io/repos/github/Castaglia/proftpd-mod_statsd/badge.svg)](https://coveralls.io/github/Castaglia/proftpd-mod_statsd)
[![License](https://img.shields.io/badge/license-GPL-brightgreen.svg)](https://img.shields.io/badge/license-GPL-brightgreen.svg)

Synopsis
--------
The `mod_statsd` module for ProFTPD emits metrics to a configured
[`statsd`](https://github.com/etsy/statsd) server, for obtaining/graphing
data rather than having to parse log files.

See the [mod_statsd.html](https://htmlpreview.github.io/?https://github.com/Castaglia/proftpd-mod_statsd/blob/master/mod_statsd.html) documentation for more
details.
